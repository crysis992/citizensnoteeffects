package net.crytec.cne;

import org.bukkit.Location;

import net.citizensnpcs.api.npc.NPC;

public class DisplayTask implements Runnable {

	private final CitizensNoteEffect plugin;
	
	public DisplayTask(CitizensNoteEffect plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public void run() {
		for (NPC npc : plugin.getNpcs().values()) {
			if (npc.isSpawned() && npc.getEntity().getWorld().isChunkLoaded(npc.getEntity().getLocation().getBlockX() >> 4, npc.getEntity().getLocation().getBlockZ() >> 4)) {
				Location loc = npc.getEntity().getLocation().clone().add(0, plugin.getYOffset(), 0);
				loc.getWorld().spawnParticle(plugin.getEffect(), loc, 1, 0, 0, 0, 0);
			}
		}
	}
}