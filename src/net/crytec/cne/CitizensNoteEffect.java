package net.crytec.cne;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import lombok.Getter;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCRemoveEvent;
import net.citizensnpcs.api.npc.NPC;
import net.crytec.api.config.PluginConfig;
import net.crytec.api.devin.commands.Command;
import net.crytec.api.devin.commands.CommandRegistrar;
import net.crytec.api.devin.commands.CommandResult;
import net.crytec.api.devin.commands.Commandable;
import net.crytec.api.util.F;

public class CitizensNoteEffect extends JavaPlugin implements Listener, Commandable {

	private static final int API_REQUIRED = 212;

	private PluginConfig config;
	private PluginConfig data;

	@Getter
	private Particle effect = Particle.NOTE;
	@Getter
	private long updateInterval = 20;
	@Getter
	private double yOffset = 1.8D;
	@Getter
	private HashMap<Integer, NPC> npcs = Maps.newHashMap();
	@Getter
	private CommandRegistrar cr;

	@SuppressWarnings("unchecked")
	@Override
	public void onEnable() {
		if (!this.checkAPIVersion()) {
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		cr = new CommandRegistrar(this);
		cr.registerCommands(this);
		cr.registerHelpCommands();

		if (!this.getDataFolder().exists()) {
			this.getDataFolder().mkdir();
		}

		config = new PluginConfig(this, getDataFolder(), "config.yml");
		config.update("particle effect", Particle.NOTE.toString());
		config.update("update interval", 20L);
		config.update("Y offset", 1.8D);
		config.saveConfig();

		data = new PluginConfig(this, getDataFolder(), "data.yml");
		this.updateInterval = config.getLong("update interval");
		this.effect = Particle.valueOf(config.getString("particle effect").toUpperCase());
		this.yOffset = config.getDouble("Y offset");

		Bukkit.getScheduler().runTaskLater(this, () -> {
			if (data.isSet("npc ids")) {
				List<Integer> list = (List<Integer>) data.get("npc ids");
				list.forEach(id -> {
					NPC npc = CitizensAPI.getNPCRegistry().getById(id);
					if (npc != null) {
						this.npcs.put(id, npc);
					}
				});
			}
		}, 120L);

		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new DisplayTask(this), 60L, updateInterval);
		Bukkit.getPluginManager().registerEvents(this, this);

	}
	
	@EventHandler
	public void onNPCRemoval(NPCRemoveEvent event) {
		if (this.npcs.containsKey(event.getNPC().getId())) {
			this.npcs.remove(event.getNPC().getId());
			this.removeFromDataList(event.getNPC().getId());
			System.out.println("Removed NPC from list");
		}
	}

	@Override
	public void onDisable() {
		data.saveConfig();
	}

	@Override
	public void reloadConfig() {
		config.reloadConfig(false);
		this.updateInterval = config.getLong("update interval");
		this.effect = Particle.valueOf(config.getString("particle effect").toUpperCase());
		this.yOffset = config.getDouble("Y offset");
	}

	@Command(struct = "cne")
	public CommandResult helpCommand(CommandSender sender) {
		return CommandResult.success(F.main("Help", "Please use /cne help to get a list of commands"));
	}

	@Command(struct = "cne set", params = "NPC ID", perms = "cne.admin", desc = "Add a new NPC to the effect list.")
	public CommandResult setEffect(CommandSender sender, int id) {
		NPC npc = CitizensAPI.getNPCRegistry().getById(id);
		if (npc == null) {
			return CommandResult.failed("There is no NPC with the ID: " + id);
		}

		if (this.npcs.containsKey(id)) {
			return CommandResult.failed("This NPC is is already on the effect list.");
		} else {
			this.npcs.put(id, npc);
			this.addToDataList(id);
		}
		return CommandResult.success(F.main("CNE", "The NPC has been successfully added to the effect list."));
	}

	@Command(struct = "cne reload", perms = "cne.admin", desc = "Reload the configuration file.")
	public CommandResult reloadConfigCommand(CommandSender sender) {
		this.reloadConfig();
		return CommandResult.success(F.main("CNE", "Sucessfully reloaded the configuration file."));
	}

	@Command(struct = "cne list", perms = "cne.admin", desc = "List all active NPCs.")
	public CommandResult listCommand(CommandSender sender) {
		sender.sendMessage(F.main("CNE", "Currently active NPCs with effects."));
		sender.sendMessage(F.format(npcs.keySet(), ",", "none"));
		return CommandResult.success();
	}

	@Command(struct = "cne remove", params = "NPC ID", perms = "cne.admin", desc = "Removes a NPC from the effect list.")
	public CommandResult removeEffect(CommandSender sender, int id) {
		if (this.npcs.containsKey(id)) {
			this.npcs.remove(id);
			this.removeFromDataList(id);
			return CommandResult.success(F.main("CNE", "The NPC has been successfully removed from the effect list."));
		} else {
			return CommandResult.failed("This NPC ID is not on the effect list.");
		}
	}
	
	private void removeFromDataList(int id) {
		ArrayList<Integer> list = (ArrayList<Integer>) data.get("npc ids", new ArrayList<Integer>());
		list.remove(Integer.valueOf(id));
		data.set("npc ids", list);
	}
	
	private void addToDataList(int id) {
		ArrayList<Integer> list = (ArrayList<Integer>) data.get("npc ids", new ArrayList<Integer>());
		list.add(id);
		data.set("npc ids", list);
	}

	public void log(String message, boolean withPrefix) {
		if (withPrefix) {
			Bukkit.getConsoleSender().sendMessage("[" + this.getDescription().getName() +"] " + message);
			return;
		}
		Bukkit.getConsoleSender().sendMessage(message);
	}

	public void log(String message) {
		log(message, false);
	}

	private boolean checkAPIVersion() {
		if (Bukkit.getPluginManager().getPlugin("CT-Core") != null) {
			String version = Bukkit.getPluginManager().getPlugin("CT-Core").getDescription().getVersion();
			version = version.replaceAll("\\.", "");
			int api_version = Integer.parseInt(version);
			if (api_version < API_REQUIRED) {
				String ver = String.valueOf(API_REQUIRED);
				log("===================================");
				log("�c Unable to load " + this.getDescription().getName() +  "!");
				log("�c You need at least version: �e" + ver.substring(0, 1) + "." + ver.substring(1, 2) + "." + ver.substring(2, 3) + "�c but you have: �6" + Bukkit.getPluginManager().getPlugin("CT-Core").getDescription().getVersion());
				log("�c Please update CT-Core to the latest version if you wish to use this plugin!");
				log("�6 Download the latest version here: https://www.spigotmc.org/resources/ct-core.24842/");
				log("===================================");
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}